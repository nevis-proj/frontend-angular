import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

import {AppElementsModule} from '@app-mod/elements/elements.module';

import {AppPagesModule} from '@app-mod/pages/pages.module';
import {PageMainHomeComponent} from '@app-mod/pages/main-home/main-home.component';
import {PagePrivacyPolicyComponent} from '@app-mod/pages/privacy-policy/privacy-policy.component';
import {PageTermsOfUseComponent} from '@app-mod/pages/terms-of-use/terms-of-use.component';
import {PageNotFoundComponent} from '@app-mod/pages/not-found/not-found.component';

import {NevisService} from '@nevis/services/nevis.service';
import {NevisBtnUserModule} from '@nevis/modules/btn-user/btn-user.module';

import {AppComponent} from './app.component';

import {AppService} from './services/app.service';
import {AppGuardAuthService} from './services/guard-auth.service';
import {AppGuardControlPanelService} from './services/guard-control-panel.service';
import {AppGuardPasswordResetReqService} from './services/guard-password-reset-req.service';
import {AppGuardCanDeactivate} from './services/guard-can-deactivate';

import {AppLayoutInfoComponent} from './layouts/info/info.component';
import {AppLayoutSiteComponent} from './layouts/site/site.component';

@NgModule({
  declarations: [
    AppComponent,

    AppLayoutInfoComponent,
    AppLayoutSiteComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    AppElementsModule,
    AppPagesModule,

    NevisBtnUserModule,

    RouterModule.forRoot([
        {
          path: '', component: AppLayoutSiteComponent, children: [
            {path: '', pathMatch: 'full', component: PageMainHomeComponent},
          ]
        },
        {
          path: 'auth',
          canLoad: [AppGuardAuthService],
          loadChildren: '../modules/app/layouts/auth/auth.module#AppLayoutAuthModule'
        },
        {
          path: 'cp',
          canLoad: [AppGuardControlPanelService],
          loadChildren: '../modules/app/layouts/control-panel/control-panel.module#AppLayoutControlPanelModule'
        },
        {
          path: 'n',
          loadChildren: '../modules/app/layouts/nevis-public/nevis-public.module#AppLayoutNevisPublicModule'
        },
        {
          path: '', component: AppLayoutInfoComponent, children: [
            {path: 'privacy-policy', component: PagePrivacyPolicyComponent},
            {path: 'terms-of-use', component: PageTermsOfUseComponent},
            {path: '**', component: PageNotFoundComponent},
          ]
        },
      ]
      // , {enableTracing: true}
    )
  ],
  providers: [
    AppService,
    NevisService,
    AppGuardAuthService,
    AppGuardControlPanelService,
    AppGuardPasswordResetReqService,
    AppGuardCanDeactivate,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
