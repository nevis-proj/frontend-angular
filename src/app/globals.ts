export enum AppLang {
  EN = 'EN',
  RU = 'RU'
}

export const APP_LANGUAGES: { value: string, viewValue: string }[] = [
  {value: AppLang.EN, viewValue: 'English'},
  {value: AppLang.RU, viewValue: 'Русский'},
];

