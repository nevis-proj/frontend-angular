import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-layout-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutInfoComponent {
}
