import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';

import {RECAPTCHA_LANGUAGE, RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';

import {DWFE_GOOGLE_CAPTCHA_INIT} from './globals';

import {DwfeRoutedTabsDirective} from './directives/routed-tabs.directive';
import {DwfeRoutedTabDirective} from './directives/routed-tab.directive';

import {DwfeAlertComponent} from './components/alert/alert.component';

import {DwfeCheckboxComponent} from './components/form-control/checkbox/checkbox.component';
import {DwfeClearControlComponent} from './components/form-control/clear-control/clear-control.component';
import {DwfeDatePickerComponent} from './components/form-control/date-picker/date-picker.component';
import {DwfeInputEmailComponent} from './components/form-control/input-email/input-email.component';
import {DwfeInputPasswordComponent} from './components/form-control/input-password/input-password.component';
import {DwfeInputTextComponent} from './components/form-control/input-text/input-text.component';
import {DwfeSelectComponent} from './components/form-control/select/select.component';
import {DwfeSlideToggleComponent} from './components/form-control/slide-toggle/slide-toggle.component';

import {DwfeGoogleCaptchaComponent} from './components/google-captcha/google-captcha.component';
import {DwfeLocationBackComponent} from './components/location-back/location-back.component';
import {DwfeOkActionComponent} from './components/action/ok/ok.component';
import {DwfeOkCancelActionComponent} from './components/action/ok-cancel/ok-cancel.component';

import {DwfeOverlayComponent} from './components/overlay/overlay.component';

import {DwfeSpinnerDottedCircleComponent} from './components/spinner/spinner-dotted-circle/spinner-dotted-circle.component';
import {DwfeSpinnerDottedHorizontalComponent} from './components/spinner/spinner-dotted-horizontal/spinner-dotted-horizontal.component';
import {DwfeSpinnerSharkFinCircleComponent} from './components/spinner/spinner-shark-fin-circle/spinner-shark-fin-circle.component';

import {DwfeCloseActionModule} from './components/action/close/close';
import {DwfeSimpleDialogModule} from './components/dialog/simple-dialog/simple-dialog.module';

@NgModule({
  declarations: [
    DwfeRoutedTabDirective,
    DwfeRoutedTabsDirective,

    DwfeAlertComponent,

    DwfeCheckboxComponent,
    DwfeClearControlComponent,
    DwfeDatePickerComponent,
    DwfeInputEmailComponent,
    DwfeInputPasswordComponent,
    DwfeInputTextComponent,
    DwfeSelectComponent,
    DwfeSlideToggleComponent,

    DwfeGoogleCaptchaComponent,
    DwfeLocationBackComponent,

    DwfeOkActionComponent,
    DwfeOkCancelActionComponent,

    DwfeOverlayComponent,

    DwfeSpinnerDottedCircleComponent,
    DwfeSpinnerDottedHorizontalComponent,
    DwfeSpinnerSharkFinCircleComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatTooltipModule,

    DwfeCloseActionModule,
    DwfeSimpleDialogModule,

    RecaptchaModule.forRoot(),
  ],
  exports: [
    RouterModule,

    DwfeRoutedTabDirective,
    DwfeRoutedTabsDirective,

    DwfeAlertComponent,

    DwfeCheckboxComponent,
    DwfeClearControlComponent,
    DwfeDatePickerComponent,
    DwfeInputEmailComponent,
    DwfeInputPasswordComponent,
    DwfeInputTextComponent,
    DwfeSelectComponent,
    DwfeSlideToggleComponent,

    DwfeGoogleCaptchaComponent,
    DwfeLocationBackComponent,

    DwfeOkActionComponent,
    DwfeOkCancelActionComponent,

    DwfeOverlayComponent,

    DwfeSpinnerDottedCircleComponent,
    DwfeSpinnerDottedHorizontalComponent,
    DwfeSpinnerSharkFinCircleComponent,

    DwfeCloseActionModule,
    DwfeSimpleDialogModule,

    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatTooltipModule,

    RecaptchaModule,
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {...DWFE_GOOGLE_CAPTCHA_INIT} as RecaptchaSettings
    },
    {
      provide: RECAPTCHA_LANGUAGE,
      useValue: 'en'
    }
  ],
})
export class DwfeModule {
}
