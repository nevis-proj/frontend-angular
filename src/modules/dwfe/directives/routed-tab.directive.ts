import {Directive} from '@angular/core';
import {RouterLink} from '@angular/router';
import {MatTab} from '@angular/material';

@Directive({
  selector: 'mat-tab[routerLink]'
})
export class DwfeRoutedTabDirective {

  constructor(public matTab: MatTab, public routerLink: RouterLink) {
  }
}
