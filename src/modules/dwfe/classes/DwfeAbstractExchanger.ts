import {HttpClient} from '@angular/common/http';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {DwfeGlobals} from '../globals';
import {DwfeExchangeable} from './DwfeAbstractExchangeableComponent';

export abstract class DwfeAbstractExchanger {

  subjResult = new Subject<DwfeExchangeResult>();

  exchangeErrorCodesMapHandlerFn: any;
  responseErrorCodesMapHandlerFn: any;
  accessToken: string;

  constructor(public http: HttpClient, public options?: any) {
    if (this.options) {
      this.accessToken = this.options['accessToken'] || '';
      this.exchangeErrorCodesMapHandlerFn = this.options['exchangeErrorCodesMapHandlerFn'];
      this.responseErrorCodesMapHandlerFn = this.options['responseErrorCodesMapHandlerFn'];
    }
  }

  abstract getHttpReq$(something?: any): Observable<Object>;

  //
  // STAGE 1.
  //
  performRequest(params?: any): DwfeAbstractExchanger {
    this.getHttpReq$(params).subscribe(
      response => this.responseHandler(response),
      error => this.errorHandler(error)
    );
    return this;
  }

  //
  // STAGE 2.
  //
  get result$(): Observable<DwfeExchangeResult> {
    return this.subjResult.asObservable();
  }

  run(initiator: DwfeExchangeable, params: any, responseHandlerFn: any): void {

    const chainExchange = params && params.chainExchange || false; // this exchange contains nested exchanges

    initiator.setErrorMessage('');          // STAGE 0. Clear error message
    initiator.setLocked(true);              // STAGE 0. Initiator goes into standby mode

    this
      .performRequest(params)               // STAGE 1. Send request
      .result$                              // STAGE 2. Get result of exchange
      .pipe(
        takeUntil(initiator.isLocked$())    // just in case, although with the current scheme it is not necessary
      )
      .subscribe(
        (data: DwfeExchangeResult) => {
          if (responseHandlerFn) {
            responseHandlerFn(data);        // STAGE 3. Process result
          }
          if (!chainExchange) {             // to avoid unsubscribing nested exchanges, if they are
            initiator.setLocked(false);
          }
        });
  }

  runGoogleCaptchaCheck(initiator: DwfeExchangeable, googleResponse: string): void {

    if (!googleResponse) {
      initiator.setCaptchaValid(false);
      return;
    }

    this.run(
      initiator,
      DwfeGlobals.prepareReq({
        googleResponse: googleResponse
      }),
      (data: DwfeExchangeResult) => {
        if (data.result) { // actions on success captcha check
          initiator.setCaptchaValid(true);
        } else {
          initiator.setErrorMessage(data.description);
        }
      }
    );
  }

  responseHandler(response): void {
    if (response['success']) {
      this.subjResult.next(DwfeExchangeResult.of({
        result: true,
        data: response['data']
      }));
    } else {
      this.subjResult.next(DwfeExchangeResult.of({
        err: this.responseErrorCodesMapHandlerFn(response)
      }));
    }
  }

  errorHandler(error): void {
    this.subjResult.next(DwfeExchangeResult.of({
      err: this.exchangeErrorCodesMapHandlerFn(error)
    }));
  }
}

export class DwfeExchangeResult {

  result: boolean;
  data: any;
  errorCode: string;
  description: any;

  static of(param): DwfeExchangeResult {
    const result = param.result || false;
    const err = param.err;

    const obj = new DwfeExchangeResult();
    obj.result = result;
    obj.data = param.data;
    if (err) {
      obj.errorCode = err.errorCode;
      obj.description = err.description;
    }
    return obj;
  }
}
