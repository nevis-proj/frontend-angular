import {HttpParameterCodec, HttpParams} from '@angular/common/http';

export class DwfeUrlEncoder implements HttpParameterCodec {

  static encode(str) {
    str = (str + '').toString();
    return encodeURIComponent(str)
      .replace(/~/g, '%7E')
      .replace(/!/g, '%21')
      .replace(/'/g, '%27')
      .replace(/\(/g, '%28')
      .replace(/\)/g, '%29')
      .replace(/\*/g, '%2A')
      .replace(/%20/g, '+');
  }

  static get encodedHttpParams(): HttpParams {
    return new HttpParams({encoder: new DwfeUrlEncoder()});
  }

  encodeKey(key: string): string {
    return key;
  }

  encodeValue(value: string): string {
    return DwfeUrlEncoder.encode(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}
