import {DwfeGlobals} from '../globals';

export abstract class DwfeAbstractStorageEntity {

  date: Date;
  instanceID: string;
  parsed: any;
  isStored = false;
  transientFields = ['transientFields', 'parsed', 'isStored'];

  constructor() {
    this.date = new Date();
    this.instanceID = DwfeGlobals.randomStr(20);
  }

  abstract get storageKey(): string;

  abstract fromStorage(params?: any): any;

  fillFromStorage(): boolean {
    try {
      const parsed = JSON.parse(localStorage.getItem(this.storageKey));
      this.date = new Date(parsed.date);
      this.instanceID = parsed.instanceID;
      this.parsed = parsed;
      return true;
    } catch (e) {
      return false;
    }
  }

  removeFromStorage(): void {
    try {
      localStorage.removeItem(this.storageKey);
    } catch (e) {
    }
  }

  saveToStorage(skip = false): void {
    if (skip) {
      return;
    }
    if (this.isAttemptToStoreModifiedObj()) {
      this.date = new Date();
      this.instanceID = DwfeGlobals.randomStr(20);
    }
    try {
      localStorage.setItem(
        this.storageKey,
        JSON.stringify(
          this,
          (key, value) => {
            if (!this.transientFields.includes(key)) {
              return value;
            }
          }));
      this.isStored = true;
    } catch (e) {
    }
  }

  isAttemptToStoreModifiedObj(): boolean {
    return this.parsed || this.isStored;
  }

  equals(obj): boolean {
    return this.instanceID === obj.instanceID;
  }
}

