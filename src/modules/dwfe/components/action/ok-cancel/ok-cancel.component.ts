import {Component, EventEmitter, Input, Output} from '@angular/core';

import {T7E_DWFE_OK_CANCEL_ACTION} from '../../../t7e';
import {AppService} from '@app/services/app.service';

@Component({
  selector: 'dwfe-ok-cancel-action',
  templateUrl: './ok-cancel.component.html',
  styleUrls: ['./ok-cancel.component.scss']
})
export class DwfeOkCancelActionComponent {

  @Input() isDisabled = false;

  @Input() showErrorMessage = false;
  @Input() showSuccessMessage = false;

  @Input() errorMessage: string;
  @Input() successMessage: string;

  @Input() LabelOk = 'Ok';

  @Output() takeOk = new EventEmitter<boolean>();
  @Output() takeCancel = new EventEmitter<boolean>();

  constructor(public app: AppService) {
  }

  get t7e(): any {
    return T7E_DWFE_OK_CANCEL_ACTION;
  }
}
