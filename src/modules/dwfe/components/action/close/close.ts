import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule} from '@angular/material';

import {DwfeCloseActionComponent} from '@dwfe/components/action/close/close.component';

@NgModule({
  declarations: [
    DwfeCloseActionComponent,
  ],
  imports: [
    CommonModule,

    MatButtonModule,
  ],
  exports: [
    DwfeCloseActionComponent,
  ]
})
export class DwfeCloseActionModule {
}
