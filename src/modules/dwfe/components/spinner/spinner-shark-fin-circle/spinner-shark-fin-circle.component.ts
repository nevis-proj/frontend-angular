import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-shark-fin-circle',
  templateUrl: './spinner-shark-fin-circle.component.html',
  styleUrls: ['./spinner-shark-fin-circle.component.scss']
})
export class DwfeSpinnerSharkFinCircleComponent {
}
