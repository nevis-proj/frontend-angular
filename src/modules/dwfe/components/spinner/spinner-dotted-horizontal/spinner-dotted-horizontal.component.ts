import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-dotted-horizontal',
  templateUrl: './spinner-dotted-horizontal.component.html',
  styleUrls: ['./spinner-dotted-horizontal.component.scss']
})
export class DwfeSpinnerDottedHorizontalComponent {
}
