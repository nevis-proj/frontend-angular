import {Component, Input} from '@angular/core';

@Component({
  selector: 'dwfe-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class DwfeAlertComponent {

  @Input() message = '';

  // Possible values:
  //    error
  //    success
  @Input() alertType = 'error';

  @Input() blink = false;
  @Input() noBorder = false;
}
