import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '../../../globals';
import {T7E_DWFE_INPUT_PASSWORD} from '../../../t7e';
import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';
import {DwfeMatErrorStateMatcher} from '../../../classes/DwfeMatErrorStateMatcher';

@Component({
  selector: 'dwfe-input-password',
  templateUrl: './input-password.component.html',
})
export class DwfeInputPasswordComponent extends DwfeAbstractEditableControl implements OnInit {

  minLength = 8;
  maxLength = 55;

  validators = [
    Validators.required,
    Validators.minLength(this.minLength),
    Validators.maxLength(this.maxLength),
    DwfeGlobals.regexValidator(new RegExp('\\d'), {'atLeastOneNumber': {}}),
    DwfeGlobals.regexValidator(new RegExp('\\D'), {'atLeastOneLetter': {}}),
  ];

  compareAs = 'textField';

  hideCharacters = true;

  matcher = new DwfeMatErrorStateMatcher();

  constructor(public app: AppService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  switchHide(): void {
    this.hideCharacters = !this.hideCharacters;
  }

  get t7e(): any {
    return T7E_DWFE_INPUT_PASSWORD;
  }
}
