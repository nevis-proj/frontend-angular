import {Component, Input, OnInit} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-select',
  templateUrl: './select.component.html',
})
export class DwfeSelectComponent extends DwfeAbstractEditableControl implements OnInit {

  @Input() items: { value: string, viewValue: any }[];

  constructor(public app: AppService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }
}
