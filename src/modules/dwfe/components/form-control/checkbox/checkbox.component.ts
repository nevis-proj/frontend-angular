import {Component, Input, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class DwfeCheckboxComponent extends DwfeAbstractEditableControl implements OnInit {

  @Input() labelLinkText = '';
  @Input() labelLinkValue = '';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }
}
