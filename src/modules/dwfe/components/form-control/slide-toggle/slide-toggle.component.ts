import {Component, Input, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-slide-toggle',
  templateUrl: './slide-toggle.component.html',
})
export class DwfeSlideToggleComponent extends DwfeAbstractEditableControl implements OnInit {

  @Input() position = 'above';
  @Input() color = 'primary';

  @Input() hintTrue = 'TRUE';
  @Input() hintFalse = 'FALSE';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  tooltipTxt(): string {
    return this.control.value ? this.hintTrue : this.hintFalse;
  }
}
