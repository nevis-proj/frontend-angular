import {Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {DwfeGlobals} from '@dwfe/globals';

@Component({
  selector: 'dwfe-simple-dialog',
  templateUrl: './simple-dialog.component.html',
  styleUrls: ['./simple-dialog.component.scss']
})
export class DwfeSimpleDialogComponent implements OnInit, OnDestroy {

  @Input() showCloseBtn = false;
  @Output() closeAction = new EventEmitter<boolean>();

  bodyClassList = document.getElementsByTagName('body')[0].classList;
  overflowClassName = 'dwfe-modal-open';
  isAddedBodyOverlayHidden = false;

  ngOnInit(): void {
    if (!this.isBodyAlreadyOverlayHidden) {
      this.bodyClassList.add(this.overflowClassName);
      this.isAddedBodyOverlayHidden = true;
    }
  }

  get isBodyAlreadyOverlayHidden(): boolean {
    return this.bodyClassList.contains(this.overflowClassName);
  }

  // == https://juristr.com/blog/2016/09/ng2-event-registration-document/
  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (this.showCloseBtn
      && DwfeGlobals.isEscapeKey(evt)) {
      this.close();
    }
  }

  close() {
    this.closeAction.emit(true);
  }

  ngOnDestroy(): void {
    if (this.isAddedBodyOverlayHidden) {
      this.bodyClassList.remove(this.overflowClassName);
    }
  }
}
