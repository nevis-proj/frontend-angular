import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatButtonModule, MatDividerModule} from '@angular/material';

import {DwfeSimpleDialogModule} from '@dwfe/components/dialog/simple-dialog/simple-dialog.module';

import {AppCopyrightComponent} from './copyright/copyright.component';
import {AppLanguageComponent} from './language/language.component';
import {AppLogoHeaderComponent} from './logo-header/logo-header.component';


@NgModule({
  declarations: [
    AppCopyrightComponent,
    AppLanguageComponent,
    AppLogoHeaderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    MatButtonModule,
    MatDividerModule,

    DwfeSimpleDialogModule,
  ],
  exports: [
    AppCopyrightComponent,
    AppLanguageComponent,
    AppLogoHeaderComponent,
  ],
})
export class AppElementsModule {
}
