import {Component, ViewEncapsulation} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {T7E_APP_LAYOUT_CONTROL_PANEL} from '../t7e';

@Component({
  selector: 'app-layout-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutControlPanelComponent {

  constructor(public app: AppService) {
  }

  get t7e(): any {
    return T7E_APP_LAYOUT_CONTROL_PANEL;
  }
}
