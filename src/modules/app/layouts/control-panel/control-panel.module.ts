import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisAccountModule} from '@nevis/modules/account/account.module';
import {NevisAccountComponent} from '@nevis/modules/account/account.component';
import {NevisAccountPersonalComponent} from '@nevis/modules/account/personal/personal.component';
import {NevisAccountEmailComponent} from '@nevis/modules/account/email/email.component';
import {NevisAccountPhoneComponent} from '@nevis/modules/account/phone/phone.component';
import {NevisAccountPasswordNDeleteComponent} from '@nevis/modules/account/password-n-delete/password-n-delete.component';

import {AppElementsModule} from '@app-mod/elements/elements.module';

import {AppPagesModule} from '@app-mod/pages/pages.module';
import {PageSettingsComponent} from '@app-mod/pages/settings/settings.component';

import {AppLayoutControlPanelComponent} from './control-panel.component';
import {AppGuardCanDeactivate} from '@app/services/guard-can-deactivate';

@NgModule({
  declarations: [
    AppLayoutControlPanelComponent
  ],
  imports: [
    CommonModule,

    AppElementsModule,
    AppPagesModule,
    DwfeModule,
    NevisAccountModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutControlPanelComponent, children: [
          {path: '', pathMatch: 'full', redirectTo: 'account'},
          {
            path: 'account', component: NevisAccountComponent, children: [
              {path: '', pathMatch: 'full', redirectTo: 'personal'},
              {path: 'personal', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountPersonalComponent},
              {path: 'email', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountEmailComponent},
              {path: 'phone', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountPhoneComponent},
              {path: 'password', component: NevisAccountPasswordNDeleteComponent},
            ]
          },
          {path: 'settings', component: PageSettingsComponent},
        ]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppLayoutControlPanelModule {
}
