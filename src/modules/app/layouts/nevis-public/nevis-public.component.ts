import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-layout-nevis-public',
  templateUrl: './nevis-public.component.html',
  styleUrls: ['./nevis-public.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutNevisPublicComponent {
}
