import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {T7E_APP_PAGE_NOT_FOUND} from '../t7e';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class PageNotFoundComponent implements OnInit {

  path: string;

  constructor(public route: ActivatedRoute,
              public app: AppService) {
  }

  ngOnInit() {
    this.route.url.subscribe(arr =>
      this.path = '/' + arr.map(segment => segment.path).join('/')
    );
  }

  get t7e(): any {
    return T7E_APP_PAGE_NOT_FOUND;
  }
}
