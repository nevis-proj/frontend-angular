import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageMainHomeComponent} from './main-home/main-home.component';
import {PageNotFoundComponent} from './not-found/not-found.component';
import {PagePrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {PageSettingsComponent} from './settings/settings.component';
import {PageTermsOfUseComponent} from './terms-of-use/terms-of-use.component';

@NgModule({
  declarations: [
    PageMainHomeComponent,
    PageNotFoundComponent,
    PagePrivacyPolicyComponent,
    PageSettingsComponent,
    PageTermsOfUseComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    PageMainHomeComponent,
    PageNotFoundComponent,
    PagePrivacyPolicyComponent,
    PageSettingsComponent,
    PageTermsOfUseComponent,
  ],
})
export class AppPagesModule {
}
