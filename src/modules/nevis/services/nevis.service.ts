import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';

import {BehaviorSubject, interval, Observable, Subject} from 'rxjs';
import {map, switchMapTo, take} from 'rxjs/operators';

import {DwfeGlobals, OPT_FOR_ANONYMOUSE_REQ} from '@dwfe/globals';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';
import {DwfeUrlEncoder} from '@dwfe/classes/DwfeUrlEncoder';
import {DwfeGoogleCaptchaValidateExchanger} from '@dwfe/classes/DwfeExchangers';

import {
  NEVIS_ENDPOINTS,
  NEVIS_OPT_FOR_TRUSTED_CLIENT,
  NEVIS_OPT_FOR_UNLIMITED_CLIENT,
  NEVIS_OPT_FOR_UNTRUSTED_CLIENT,
  NevisClientType,
  NevisGlobals
} from '../globals';
import {NevisAccountAccessBackendEntity} from '../classes/NevisAccountAccessBackendEntity';
import {NevisAccountEmailBackendEntity} from '../classes/NevisAccountEmailBackendEntity';
import {NevisAccountPhoneBackendEntity} from '../classes/NevisAccountPhoneBackendEntity';
import {NevisAccountPersonalBackendEntity} from '../classes/NevisAccountPersonalBackendEntity';
import {
  NevisAccountPublicInfoExchanger,
  NevisCreateAccountExchanger,
  NevisDeleteAccountExchanger,
  NevisEmailChangeExchanger,
  NevisEmailConfirmExchanger,
  NevisEmailConfirmReqExchanger,
  NevisGetAccountEmailExchanger,
  NevisGetAccountPersonalExchanger,
  NevisGetAccountPhoneExchanger,
  NevisNickNameChangeExchanger,
  NevisPasswordChangeExchanger,
  NevisPasswordResetExchanger,
  NevisPasswordResetReqExchanger,
  NevisPhoneChangeExchanger,
  NevisThirdPartyAuthExchanger,
  NevisUpdateAccountEmailExchanger,
  NevisUpdateAccountPersonalExchanger,
  NevisUpdateAccountPhoneExchanger
} from '../classes/NevisExchangers';
import {NEVIS_BACKEND_EMAIL_VALIDATOR} from '../t7e';

@Injectable({
  providedIn: 'root'
})
export class NevisService {

  subjLoggedIn = new BehaviorSubject<boolean>(false);
  subjLoginResult = new Subject<DwfeExchangeResult>();

  auth: NevisAccountAccessBackendEntity;

  _accountEmail: NevisAccountEmailBackendEntity;
  subjAccountEmail = new Subject<NevisAccountEmailBackendEntity>();

  _accountPhone: NevisAccountPhoneBackendEntity;
  subjAccountPhone = new Subject<NevisAccountPhoneBackendEntity>();

  _accountPersonal: NevisAccountPersonalBackendEntity;
  subjAccountPersonal = new Subject<NevisAccountPersonalBackendEntity>();

  redirectUrl: string;

  exchangeErrorCodesMapHandler = DwfeGlobals.parseExchangeError;
  responseErrorCodesMapHandler = NevisGlobals.parseExchangeError;

  constructor(public http: HttpClient,
              public router: Router) {
    this.init();
  }

  init(): void {

    this.auth = new NevisAccountAccessBackendEntity().fromStorage(this);
    this.LoggedIn = !!this.auth;

    if (this.isLoggedIn) {
      this._accountEmail = new NevisAccountEmailBackendEntity().fromStorage();
      this._accountPhone = new NevisAccountPhoneBackendEntity().fromStorage();
      this._accountPersonal = new NevisAccountPersonalBackendEntity().fromStorage();
    }
  }

  setRedirectUrl(value: string): NevisService {
    this.redirectUrl = value;
    return this;
  }

  coverUpOnesTraces() {
    if (this.auth) {
      this.auth.removeFromStorage();
      this.auth = null;
    }
    if (this._accountEmail) {
      this._accountEmail.removeFromStorage();
      this._accountEmail = null;
    }
    if (this._accountPhone) {
      this._accountPhone.removeFromStorage();
      this._accountPhone = null;
    }
    if (this._accountPersonal) {
      this._accountPersonal.removeFromStorage();
      this._accountPersonal = null;
    }
  }

  get isLoggedIn(): boolean {
    return this.subjLoggedIn.getValue();
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.subjLoggedIn.asObservable();
  }

  set LoggedIn(value: boolean) {
    this.subjLoggedIn.next(value);
  }

  login(): void {
    this.LoggedIn = true;
    this.subjLoginResult.next(DwfeExchangeResult.of({result: true}));
    if (this.redirectUrl) {
      setTimeout(() => {
        this.router.navigateByUrl(this.redirectUrl);
        this.redirectUrl = null;
      }, 50);
    }
  }

  get loginResult$(): Observable<DwfeExchangeResult> {
    return this.subjLoginResult.asObservable();
  }

  logout(): void {
    this.http
      .get(
        NEVIS_ENDPOINTS.signOut,
        DwfeGlobals.optForAuthorizedReq(this.auth.accessToken))
      .subscribe(
        data => { // I already did everything I could
        },
        error => { // I already did everything I could
        }
      );
    setTimeout(() => {
      this.LoggedIn = false;
      this.router.navigateByUrl('/');
      this.coverUpOnesTraces();
    }, 300);
  }

  performLoginStandard(username: string, password: string, oauth2ClientType: string): NevisService {

    let opt = null;
    if (oauth2ClientType === NevisClientType.UNTRUSTED) {
      opt = NEVIS_OPT_FOR_UNTRUSTED_CLIENT;
    } else if (oauth2ClientType === NevisClientType.TRUSTED) {
      opt = NEVIS_OPT_FOR_TRUSTED_CLIENT;
    } else if (oauth2ClientType === NevisClientType.UNLIMITED) {
      opt = NEVIS_OPT_FOR_UNLIMITED_CLIENT;
    }

    this.http
      .post(
        NEVIS_ENDPOINTS.signIn,
        DwfeUrlEncoder.encodedHttpParams
          .set('grant_type', 'password')
          .set('username', username)
          .set('password', password)
          .set('usernameType', 'EMAIL'),
        opt)
      .subscribe(
        response => this.performLogin(response, oauth2ClientType),
        error => this.subjLoginResult.next(DwfeExchangeResult.of({
          err: DwfeGlobals.parseExchangeError(error)
        }))
      );
    return this;
  }

  performLogin(signInResp: any, oauth2ClientType: string) {
    this.auth = NevisAccountAccessBackendEntity.of(signInResp, this, oauth2ClientType);
    this.login();
  }

  isOAuthClientUntrusted(): boolean {
    if (!this.auth || !this.auth.oauth2ClientType) {
      return true;
    }
    return this.auth.oauth2ClientType === NevisClientType.UNTRUSTED;
  }

  performTokenRefresh(authFromThePast: NevisAccountAccessBackendEntity): void {

    // Update the token only in case:
    if (this.isLoggedIn                      // 1. Is logged in
      && authFromThePast.equals(this.auth)   // 2. The time has come to update the CURRENT token
    ) {
      this.http
        .post(
          NEVIS_ENDPOINTS.tokenRefresh,
          new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('refresh_token', this.auth.refreshToken),
          NEVIS_OPT_FOR_TRUSTED_CLIENT)
        .subscribe(
          response => {
            this.auth = NevisAccountAccessBackendEntity.of(response, this, authFromThePast.oauth2ClientType);
          },
          error => {
            if (DwfeGlobals.isReasonForLogoutHttpErrResp(error)) {
              this.logout();
            } else {
              const time = this.auth.get95PercentOfExpiryTime();
              if (time > 10 * 1000) { // if 95% percent of token valid time > 10 seconds
                this.auth.scheduleTokenUpdate(this, time);
              } else {
                // Let's analyze:
                //  1) access_token refresh FAILED
                //  2) before the access_token expires, there are LESS THAN 10 seconds left
                // ==>> of course, you must Log-out
                this.logout();
              }
            }
          }
        );
    }
  }

  set accountEmail(dataFromBackend: any) {
    const accountEmail = NevisAccountEmailBackendEntity.of(dataFromBackend, this.isOAuthClientUntrusted());
    this._accountEmail = accountEmail;
    this.subjAccountEmail.next(accountEmail);
  }

  setEmailConfirmed(value: boolean): void {
    if (this._accountEmail) {
      this._accountEmail.confirmed = value;
      this._accountEmail.saveToStorage(this.isOAuthClientUntrusted());
      this.subjAccountEmail.next(this._accountEmail);
    }
  }

  isEmailConfirmed(): boolean {
    return this._accountEmail && this._accountEmail.confirmed;
  }

  syncConfirmedEmailFlagWithStorage() {
    if (this._accountEmail && this._accountEmail.isConfirmedDifferentInStorage()) {
      this.subjAccountEmail.next(this._accountEmail);
    }
  }

  accountEmail$(params?: any): Observable<NevisAccountEmailBackendEntity> {

    if (this._accountEmail) {     // (1) from local
      setTimeout(() => {
        this.subjAccountEmail.next(this._accountEmail);
      }, 50); // because the Observable will be returned below
    } else {
      if (params) {               // (2) request by user directly from frontend
        this.getAccountEmailExchanger.run(
          params['initiator'],
          params['params'],
          params['responseHandlerFn']
        );
      } else {                    // (3) non-visual request, just get data
        const subscription = this.getAccountEmailExchanger
          .performRequest()
          .result$
          .subscribe(
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.accountEmail = data.data;
              } else {
                // actions? nowhere to report an error
              }
              subscription.unsubscribe();
            });
      }
    }
    return this.subjAccountEmail.asObservable();
  }

  updateAccountEmail(obj: any): void {

    if (!this._accountEmail) {
      this.accountEmail$();
      return;
    }

    for (const [key, value] of Object.entries(obj)) {
      this._accountEmail[key] = value;
    }
    this._accountEmail.saveToStorage(this.isOAuthClientUntrusted());
    this.subjAccountEmail.next(this._accountEmail);
  }

  refreshAccountEmail(params?: any): void {
    if (this._accountEmail) {
      this._accountEmail.removeFromStorage();
      this._accountEmail = null;
    }
    this.accountEmail$(params);
  }

  set accountPhone(dataFromBackend: any) {
    const accountPhone = NevisAccountPhoneBackendEntity.of(dataFromBackend, this.isOAuthClientUntrusted());
    this._accountPhone = accountPhone;
    this.subjAccountPhone.next(accountPhone);
  }

  accountPhone$(params?: any): Observable<NevisAccountPhoneBackendEntity> {

    if (this._accountPhone) {     // (1) from local
      setTimeout(() => {
        this.subjAccountPhone.next(this._accountPhone);
      }, 50); // because the Observable will be returned below
    } else {
      if (params) {               // (2) request by user directly from frontend
        this.getAccountPhoneExchanger.run(
          params['initiator'],
          null,
          params['responseHandlerFn']
        );
      } else {                    // (3) non-visual request, just get data
        const subscription = this.getAccountPhoneExchanger
          .performRequest()
          .result$
          .subscribe(
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.accountPhone = data.data;
              } else {
                // actions? nowhere to report an error
              }
              subscription.unsubscribe();
            });
      }
    }
    return this.subjAccountPhone.asObservable();
  }

  updateAccountPhone(obj: any): void {

    if (!this._accountPhone) {
      this.accountPhone$();
      return;
    }

    for (const [key, value] of Object.entries(obj)) {
      this._accountPhone[key] = value;
    }
    this._accountPhone.saveToStorage(this.isOAuthClientUntrusted());
    this.subjAccountPhone.next(this._accountPhone);
  }

  set accountPersonal(dataFromBackend: any) {
    const accountPersonal = NevisAccountPersonalBackendEntity.of(dataFromBackend, this.isOAuthClientUntrusted());
    this._accountPersonal = accountPersonal;
    this.subjAccountPersonal.next(accountPersonal);
  }

  accountPersonal$(params?: any): Observable<NevisAccountPersonalBackendEntity> {

    if (this._accountPersonal) {  // (1) from local
      setTimeout(() => {
        this.subjAccountPersonal.next(this._accountPersonal);
      }, 50); // because the Observable will be returned below
    } else {
      if (params) {               // (2) request by user directly from frontend
        this.getAccountPersonalExchanger.run(
          params['initiator'],
          null,
          params['responseHandlerFn']
        );
      } else {                    // (3) non-visual request, just get data
        const subscription = this.getAccountPersonalExchanger
          .performRequest()
          .result$
          .subscribe(
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.accountPersonal = data.data;
              } else {
                // actions? nowhere to report an error
              }
              subscription.unsubscribe();
            });
      }
    }
    return this.subjAccountPersonal.asObservable();
  }

  updateAccountPersonal(obj: any): void {

    if (!this._accountPersonal) {
      this.accountPersonal$();
      return;
    }

    for (const [key, value] of Object.entries(obj)) {
      this._accountPersonal[key] = value;
    }
    this._accountPersonal.saveToStorage(this.isOAuthClientUntrusted());
    this.subjAccountPersonal.next(this._accountPersonal);
  }


  //
  // BACKEND VALIDATORS
  //

  backendValidatorEmail(email: string, reverseHandleResp: boolean) {

    // Don't send request to the backend after each press on the keyboard.
    // Only the last result$ for the interval.
    // Based on: https://github.com/angular/angular/issues/6895#issuecomment-329464982
    const debounceTime = 500; // ms

    if (reverseHandleResp) {      // e.g. for 'Login'

      return interval(debounceTime)
        .pipe(
          switchMapTo(this.post_canUseUsername(email)),
          map(
            response => {
              if (response['success']) {
                return {
                  'backendHttp': NEVIS_BACKEND_EMAIL_VALIDATOR.not_found_in_db
                };
              }
              return null;
            },
            error => {
              return {'backendHttp': this.exchangeErrorCodesMapHandler(error).description};
            }),
          take(1)
        );

    } else {                      // e.g. for 'Create Account'

      return interval(debounceTime)
        .pipe(
          switchMapTo(this.post_canUseUsername(email)),
          map(
            response => {
              if (!response['success']) {
                return {'backendHttp': this.responseErrorCodesMapHandler(response).description};
              }
              return null;
            },
            error => {
              return {'backendHttp': this.exchangeErrorCodesMapHandler(error).description};
            }),
          take(1)
        );
    }
  }


  //
  // EXCHANGERS
  //

  getExchangeOptV1() {
    return {
      exchangeErrorCodesMapHandlerFn: this.exchangeErrorCodesMapHandler,
      responseErrorCodesMapHandlerFn: this.responseErrorCodesMapHandler
    };
  }

  getExchangeOptV2() {
    return {
      accessToken: this.auth.accessToken,
      exchangeErrorCodesMapHandlerFn: this.exchangeErrorCodesMapHandler,
      responseErrorCodesMapHandlerFn: this.responseErrorCodesMapHandler
    };
  }

  post_canUseUsername(email: string): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.canUseUsername,
      DwfeGlobals.prepareReq({
        username: email,
        usernameType: 'EMAIL'
      }),
      OPT_FOR_ANONYMOUSE_REQ);
  }


  // DWFE

  get googleCaptchaValidateExchanger(): DwfeGoogleCaptchaValidateExchanger {
    return new DwfeGoogleCaptchaValidateExchanger(this.http, this.getExchangeOptV1());
  }


  // Account.Common

  get createAccountExchanger(): NevisCreateAccountExchanger {
    return new NevisCreateAccountExchanger(this.http, this.getExchangeOptV1());
  }

  get publicAccountInfoExchanger(): NevisAccountPublicInfoExchanger {
    return new NevisAccountPublicInfoExchanger(this.http, this.getExchangeOptV1());
  }

  get deleteAccountExchanger(): NevisDeleteAccountExchanger {
    return new NevisDeleteAccountExchanger(this.http, this.getExchangeOptV2());
  }

  get thirdPartyAuthExchanger(): NevisThirdPartyAuthExchanger {
    return new NevisThirdPartyAuthExchanger(this.http, this.getExchangeOptV1());
  }


  // Account.Access

  get passwordChangeExchanger(): NevisPasswordChangeExchanger {
    return new NevisPasswordChangeExchanger(this.http, this.getExchangeOptV2());
  }

  get passwordResetReqExchanger(): NevisPasswordResetReqExchanger {
    return new NevisPasswordResetReqExchanger(this.http, this.getExchangeOptV1());
  }

  get passwordResetExchanger(): NevisPasswordResetExchanger {
    return new NevisPasswordResetExchanger(this.http, this.getExchangeOptV1());
  }


  // Account.Email

  get getAccountEmailExchanger(): NevisGetAccountEmailExchanger {
    return new NevisGetAccountEmailExchanger(this.http, this.getExchangeOptV2());
  }

  get emailConfirmReqExchanger(): NevisEmailConfirmReqExchanger {
    return new NevisEmailConfirmReqExchanger(this.http, this.getExchangeOptV2());
  }

  get emailConfirmExchanger(): NevisEmailConfirmExchanger {
    return new NevisEmailConfirmExchanger(this.http, this.getExchangeOptV1());
  }

  get emailChangeExchanger(): NevisEmailChangeExchanger {
    return new NevisEmailChangeExchanger(this.http, this.getExchangeOptV2());
  }

  get updateAccountEmailExchanger(): NevisUpdateAccountEmailExchanger {
    return new NevisUpdateAccountEmailExchanger(this.http, this.getExchangeOptV2());
  }


  // Account.Phone

  get getAccountPhoneExchanger(): NevisGetAccountPhoneExchanger {
    return new NevisGetAccountPhoneExchanger(this.http, this.getExchangeOptV2());
  }

  get phoneChangeExchanger(): NevisPhoneChangeExchanger {
    return new NevisPhoneChangeExchanger(this.http, this.getExchangeOptV2());
  }

  get updateAccountPhoneExchanger(): NevisUpdateAccountPhoneExchanger {
    return new NevisUpdateAccountPhoneExchanger(this.http, this.getExchangeOptV2());
  }


  // Account.Personal

  get getAccountPersonalExchanger(): NevisGetAccountPersonalExchanger {
    return new NevisGetAccountPersonalExchanger(this.http, this.getExchangeOptV2());
  }

  get nickNameChangeExchanger(): NevisNickNameChangeExchanger {
    return new NevisNickNameChangeExchanger(this.http, this.getExchangeOptV2());
  }

  get updateAccountPersonalExchanger(): NevisUpdateAccountPersonalExchanger {
    return new NevisUpdateAccountPersonalExchanger(this.http, this.getExchangeOptV2());
  }
}
