import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

export class NevisAccountEmailBackendEntity extends DwfeAbstractStorageEntity {

  value: string;
  nonPublic: boolean;
  confirmed: boolean;
  updatedOn: Date;

  isSentConfirmReq: boolean;

  get storageKey(): string {
    return 'nevisAccountEmail';
  }

  static of(data, isOAuthClientUntrusted: boolean): NevisAccountEmailBackendEntity {
    const obj = new NevisAccountEmailBackendEntity();

    obj.value = data['value'];
    obj.nonPublic = data['nonPublic'];
    obj.confirmed = data['confirmed'];
    obj.updatedOn = DwfeGlobals.dateFromObj(data, 'updatedOn');

    obj.isSentConfirmReq = false;

    obj.saveToStorage(isOAuthClientUntrusted);
    return obj;
  }

  fromStorage(params?: any): any {

    if (this.fillFromStorage()) {

      this.value = this.parsed.value;
      this.nonPublic = this.parsed.nonPublic;
      this.confirmed = this.parsed.confirmed;
      this.updatedOn = DwfeGlobals.dateFromObj(this.parsed, 'updatedOn');

      this.isSentConfirmReq = this.parsed.isSentConfirmReq;

      return this;
    } else {
      return null;
    }
  }

  isConfirmedDifferentInStorage() {
    try {
      const parsed = JSON.parse(localStorage.getItem(this.storageKey));
      if (this.confirmed === parsed.confirmed) {
        return false;
      } else {
        this.confirmed = parsed.confirmed;
        return true;
      }
    } catch (e) {
      return false;
    }
  }
}
