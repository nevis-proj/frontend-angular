import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

export class NevisAccountPhoneBackendEntity extends DwfeAbstractStorageEntity {

  value: string;
  nonPublic: boolean;
  confirmed: boolean;
  updatedOn: Date;

  get storageKey(): string {
    return 'nevisAccountPhone';
  }

  static of(data, isOAuthClientUntrusted: boolean): NevisAccountPhoneBackendEntity {
    const obj = new NevisAccountPhoneBackendEntity();

    obj.value = data['value'];
    obj.nonPublic = data['nonPublic'];
    obj.confirmed = data['confirmed'];
    obj.updatedOn = DwfeGlobals.dateFromObj(data, 'updatedOn');

    obj.saveToStorage(isOAuthClientUntrusted);
    return obj;
  }

  fromStorage(params?: any): any {

    if (this.fillFromStorage()) {

      this.value = this.parsed.value;
      this.nonPublic = this.parsed.nonPublic;
      this.confirmed = this.parsed.confirmed;
      this.updatedOn = DwfeGlobals.dateFromObj(this.parsed, 'updatedOn');

      return this;
    } else {
      return null;
    }
  }
}
