import {Observable} from 'rxjs';

import {DwfeGlobals, OPT_FOR_ANONYMOUSE_REQ} from '@dwfe/globals';
import {DwfeAbstractExchanger} from '@dwfe/classes/DwfeAbstractExchanger';

import {NEVIS_ENDPOINTS} from '../globals';


// Account.Common

export class NevisCreateAccountExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.createAccount,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}

export class NevisAccountPublicInfoExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.get(
      NEVIS_ENDPOINTS.id + '/' + params.id,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}

export class NevisDeleteAccountExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.deleteAccount,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisThirdPartyAuthExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.thirdPartyAuth,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}


// Account.Access

export class NevisPasswordChangeExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.passwordChange,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisPasswordResetReqExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.passwordResetReq,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}

export class NevisPasswordResetExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.passwordReset,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}


// Account.Email

export class NevisGetAccountEmailExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.get(
      NEVIS_ENDPOINTS.getAccountEmail,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisEmailConfirmReqExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.get(
      NEVIS_ENDPOINTS.emailConfirmReq,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisEmailConfirmExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.emailConfirm,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}

export class NevisEmailChangeExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.emailChange,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisUpdateAccountEmailExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.updateAccountEmail,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}


// Account.Phone

export class NevisGetAccountPhoneExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.get(
      NEVIS_ENDPOINTS.getAccountPhone,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisPhoneChangeExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.phoneChange,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisUpdateAccountPhoneExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.updateAccountPhone,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}


// Account.Personal

export class NevisGetAccountPersonalExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.get(
      NEVIS_ENDPOINTS.getAccountPersonal,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisNickNameChangeExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.nickNameChange,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}

export class NevisUpdateAccountPersonalExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      NEVIS_ENDPOINTS.updateAccountPersonal,
      body,
      DwfeGlobals.optForAuthorizedReq(this.accessToken));
  }
}
