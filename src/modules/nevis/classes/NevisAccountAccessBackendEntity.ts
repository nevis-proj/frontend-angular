import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

import {NevisClientType} from '../globals';
import {NevisService} from '../services/nevis.service';

export class NevisAccountAccessBackendEntity extends DwfeAbstractStorageEntity {

  accessToken: string;
  expiresIn: number;
  refreshToken: string;

  id: number;
  username: string;
  thirdParty: string;
  hasRoleAdmin: boolean;
  hasRoleUser: boolean;

  oauth2ClientType: string;

  createdOn: Date;

  get storageKey(): string {
    return 'nevisAccountAccess';
  }

  static of(data: any, nevisService: NevisService, oauth2ClientType: string): NevisAccountAccessBackendEntity {
    const obj = new NevisAccountAccessBackendEntity();
    const access = data['data'];
    let hasRoleAdmin = false;
    let hasRoleUser = false;

    obj.accessToken = data['access_token'];
    if (data['expires_in']) { // for unlimited access_token 'expires_in' is missing
      obj.expiresIn = Date.now() + data['expires_in'] * 1000;
    }
    obj.refreshToken = data['refresh_token'];

    access['authorities'].forEach(next => {
      if (next === 'ADMIN') {
        hasRoleAdmin = true;
      } else if (next === 'USER') {
        hasRoleUser = true;
      }
    });

    obj.id = +access['id'];
    obj.username = access['username'];
    obj.thirdParty = access['thirdParty'];
    obj.hasRoleAdmin = hasRoleAdmin;
    obj.hasRoleUser = hasRoleUser;

    obj.createdOn = DwfeGlobals.dateFromObj(access, 'createdOn');

    obj.oauth2ClientType = oauth2ClientType;

    obj.saveToStorage(oauth2ClientType === NevisClientType.UNTRUSTED);

    if (oauth2ClientType === NevisClientType.TRUSTED) {
      obj.scheduleTokenUpdate(nevisService, obj.get95PercentOfExpiryTime());
    } else if (oauth2ClientType === NevisClientType.UNTRUSTED) {
      obj.scheduleLogout(nevisService, obj.getExpiryTime());
    }
    return obj;
  }

  fromStorage(nevisService: NevisService): NevisAccountAccessBackendEntity {
    if (this.fillFromStorage()) {

      this.oauth2ClientType = this.parsed.oauth2ClientType;

      if (this.oauth2ClientType === NevisClientType.TRUSTED) {
        this.expiresIn = +this.parsed.expiresIn;
        if (this.expiresIn < Date.now()) {
          return null;
        }
      }

      this.accessToken = this.parsed.accessToken;
      this.refreshToken = this.parsed.refreshToken;

      this.id = +this.parsed.id;
      this.username = this.parsed.username;
      this.thirdParty = this.parsed.thirdParty;
      this.hasRoleAdmin = this.parsed.hasRoleAdmin;
      this.hasRoleUser = this.parsed.hasRoleUser;

      this.createdOn = DwfeGlobals.dateFromObj(this.parsed, 'createdOn');

      if (this.oauth2ClientType === NevisClientType.TRUSTED) {
        const time = this.get95PercentOfExpiryTime();
        const time1Day = (60 * 60 * 24) * 1000;
        if (time < time1Day) {
          this.scheduleTokenUpdate(nevisService, 10 * 1000);
        } else {
          this.scheduleTokenUpdate(nevisService, time);
        }
      }
      return this;
    } else {
      return null;
    }
  }

  get95PercentOfExpiryTime(): number {
    return Math.round(this.getExpiryTime() * 0.95);
  }

  getExpiryTime(): number {
    return this.expiresIn - Date.now();
  }

  scheduleTokenUpdate(nevisService: NevisService, time: number): void {
    if (time >= 0) {
      setTimeout(() => {
        nevisService.performTokenRefresh(this);
      }, time);
    }
  }

  scheduleLogout(nevisService: NevisService, time: number): void {
    if (time >= 0) {
      setTimeout(() => {
        nevisService.logout();
      }, time);
    }
  }
}
