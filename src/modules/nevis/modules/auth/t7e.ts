export const T7E_NEVIS_AUTH_CREATE_ACCOUNT = {
  catcha_title: {
    EN: 'Please confirm that you are a real person:',
    RU: 'Подтвердите, что вы человек:',
  },
  email: {
    EN: 'Your e-mail',
    RU: 'Ваша эл. почта',
  },
  password_will_be_sent: {
    EN: '*Password will be sent to your e-mail',
    RU: '*Пароль будет отправлен на вашу почту',
  },
  i_accept: {
    EN: 'I accept ',
    RU: 'Я принимаю ',
  },
  terms: {
    EN: 'Terms of use',
    RU: 'Условия',
  },
  privacy: {
    EN: 'Privacy policy',
    RU: 'Политику',
  },
  was_success_created: {
    EN: 'Your account was successfully created',
    RU: 'Ваш аккаунт был успешно создан',
  },
  password_we_sent: {
    EN: 'Password we sent to your e-mail',
    RU: 'Пароль мы отправили вам на почту',
  },
  check_email_box: {
    EN: 'Please check your e-mail box',
    RU: 'Пожалуйста проверьте свой почтовый ящик',
  },
  go_to_login: {
    EN: 'Go to Log-in',
    RU: 'Вход',
  },
};

export const T7E_NEVIS_AUTH_LOGIN = {
  or: {
    EN: 'or',
    RU: 'или',
  },
  email: {
    EN: 'E-mail',
    RU: 'Эл. почта',
  },
  password: {
    EN: 'Password',
    RU: 'Пароль',
  },
  not_my_device: {
    EN: 'That\'s not my personal device',
    RU: 'Чужой компьютер',
  },
  forgot_password: {
    EN: 'Forgot password?',
    RU: 'Забыли пароль?',
  },
  successfully_logged_in: {
    EN: 'You have successfully logged-in',
    RU: 'Вход в систему произведен',
  },
  go_to_index: {
    EN: 'Go to Main page',
    RU: 'На Главную',
  },
};

export const T7E_NEVIS_AUTH_THIRD_PARTY = {
  title: {
    EN: 'Log in through a third-party provider',
    RU: 'Войти через стороннего поставщика',
  },
};
