import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import {DwfeGlobals} from '@dwfe/globals';

import {NEVIS_GOOGLE_AUTH_INIT_PARAMS} from '../../../../globals';

declare let gapi; // will appear in the DOM after loading <script>

@Component({
  selector: 'nevis-auth-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.scss']
})
export class NevisAuthGoogleComponent implements OnInit {

  googleAuth;

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  ngOnInit() {
    DwfeGlobals.appendChildScript(
      '//apis.google.com/js/platform.js',
      true,
      document.getElementsByTagName('nevis-auth-google').item(0),
      () => {
        gapi
          .load('auth2', () => { // https://developers.google.com/identity/sign-in/web/reference#auth-setup
            gapi.auth2
              .init({                // https://developers.google.com/identity/sign-in/web/reference#gapiauth2initparams
                ...NEVIS_GOOGLE_AUTH_INIT_PARAMS
              })
              .then(                 // https://developers.google.com/identity/sign-in/web/reference#googleauththenoninit-onerror
                googleAuth => {
                  this.googleAuth = googleAuth; // now GogleAuth is fully initialized

                  this.isDisabled = false;
                  DwfeGlobals.triggerChangeDetection();
                },
                error => {
                  // on init error 'isDisabled' still remains equals to 'true'
                });
          });
      }
    );
    this.signInResult.emit(null);
  }

  performSignInOnGoogleServer() {
    const signInResult = this.googleAuth.signIn(); // https://developers.google.com/identity/sign-in/web/reference#googleauthsignin

    signInResult.then(
      googleUser => {
        this.signInResult.emit({
            thirdParty: this,
            req: {
              identityCheckData: googleUser.getAuthResponse().id_token, // https://developers.google.com/identity/sign-in/web/backend-auth#send-the-id-token-to-your-server
              thirdParty: 'GOOGLE',
              email: googleUser.getBasicProfile().getEmail(),
            }
          }
        );
      },
      error => {
      });
  }

  performSignOut() {
    this.googleAuth.signOut();
  }
}
