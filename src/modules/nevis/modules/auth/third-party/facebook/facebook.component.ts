import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import {DwfeGlobals} from '@dwfe/globals';

declare let FB; // will appear in the DOM after loading <script>

@Component({
  selector: 'nevis-auth-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.scss']
})
export class NevisAuthFacebookComponent implements OnInit {

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  ngOnInit() {
    DwfeGlobals.appendChildScript(
      '//connect.facebook.net/en_US/sdk.js', // https://developers.facebook.com/docs/javascript/quickstart
      false,
      document.getElementsByTagName('nevis-auth-facebook').item(0),
      () => {
        FB.init({ // https://developers.facebook.com/docs/facebook-login/web#example
          appId: '167940340684275', // https://developers.facebook.com/apps/
          cookie: false,  // enable cookies to allow the server to access the session
          xfbml: false,   // parse social plugins on this page
          version: 'v3.1' // https://developers.facebook.com/docs/apps/versions/#versioning
        });

        this.isDisabled = false;
        DwfeGlobals.triggerChangeDetection();
      }
    );
    this.signInResult.emit(null);
  }

  performSignInOnFacebookServer() {

    FB.login(
      respLogin => { // https://developers.facebook.com/docs/facebook-login/web#dialogresponse
        if (respLogin.status === 'connected') { // Logged into your app and Facebook
          FB.api('/me?fields=email',
            respApi => {
              this.signInResult.emit({
                thirdParty: this,
                req: {
                  identityCheckData: respLogin.authResponse.accessToken, // https://developers.facebook.com/docs/facebook-login/web#checklogin
                  thirdParty: 'FACEBOOK',
                  email: respApi.email,
                }
              });
            });
        }
      }, {
        scope: 'email',        // https://developers.facebook.com/docs/facebook-login/web/permissions
        auth_type: 'rerequest'
      }
    );
  }

  performSignOut() {
    FB.logout();
  }
}
