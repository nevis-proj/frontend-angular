import {Component, EventEmitter, OnInit, Output} from '@angular/core';

declare let VK; // will appear in the DOM after loading <script>

@Component({
  selector: 'nevis-auth-vkontakte',
  templateUrl: './vkontakte.component.html',
  styleUrls: ['./vkontakte.component.scss']
})
export class NevisAuthVkontakteComponent implements OnInit {

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  ngOnInit() {

    // DwfeGlobals.appendChildScript(
    //   '//vk.com/js/api/openapi.js?159', // https://vk.com/dev/openapi?f=2.2.%20Асинхронная%20инициализация
    //   false,
    //   document.getElementsByTagName('nevis-auth-vkontakte').item(0),
    //   () => {
    //     VK.init({
    //       apiId: '{your app ID}', // https://vk.com/apps?act=manage
    //     });
    //   }
    // );
    this.signInResult.emit(null);
  }

  performSignInOnVkontakteServer() {

    VK.Auth.login(
      respLogin => {
        if (respLogin.status === 'connected') {
          VK.Api.call( // https://vk.com/dev/api_requests?f=4.3.%20Запрос%20с%20использованием%20Open%20API
            'users.get', { // https://vk.com/dev/users.get
              user_id: respLogin.session.mid,
              fields: 'photo_max,contacts',
              v: '5.80'
            },
            respApi => {
              // Походу email можно получить только используя метод авторизации с редиректом:
              // https://vk.com/dev/auth_sites
              // https://stackoverflow.com/questions/25528771/how-to-get-email-address-from-vk-api#36932000
              // https://toster.ru/q/494852
              // и даже, если заморочиться, то email может отсутствовать.
            }
          );
        }
      });
  }

  performSignOut() {
  }
}
