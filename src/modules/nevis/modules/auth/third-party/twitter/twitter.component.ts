import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'nevis-auth-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class NevisAuthTwitterComponent implements OnInit {

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  ngOnInit() {
    this.signInResult.emit(null);
  }

  performSignOut() {
  }
}
