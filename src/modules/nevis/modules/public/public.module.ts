import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisAccountPublicInfoComponent} from './info/public-info.component';
import {NevisEmailConfirmComponent} from './email-confirm/confirm.component';
import {NevisPasswordResetReqComponent} from './password-reset/request/request.component';
import {NevisPasswordResetComponent} from './password-reset/password-reset.component';

@NgModule({
  imports: [
    CommonModule,

    DwfeModule,
  ],
  declarations: [
    NevisAccountPublicInfoComponent,
    NevisEmailConfirmComponent,
    NevisPasswordResetReqComponent,
    NevisPasswordResetComponent,
  ],
  exports: [
    NevisAccountPublicInfoComponent,
    NevisEmailConfirmComponent,
    NevisPasswordResetReqComponent,
    NevisPasswordResetComponent,
  ]
})
export class NevisPublicModule {
}
