export const T7E_NEVIS_EMAIL_CONFIRM = {
  title: {
    EN: 'E-mail confirm',
    RU: 'Подтверждение эл. почты',
  },
  error: {
    EN: 'Error',
    RU: 'Ошибка',
  },
  email_already_confirmed: {
    EN: 'Your e-mail is already confirmed',
    RU: 'Ваша эл. почта уже подтверждена',
  },
  email_now_confirmed: {
    EN: 'Your e-mail is now confirmed',
    RU: 'Теперь ваша эл. почта подтверждена',
  },
};

export const T7E_NEVIS_ACCOUNT_PUBLIC_INFO = {
  title: {
    EN: 'Public information',
    RU: 'Открытая информация',
  },
  refresh: {
    EN: 'Refresh',
    RU: 'Обновить',
  },
  hidden_information: {
    EN: 'hidden information',
    RU: 'информация скрыта',
  },
  param_id: {
    EN: 'id',
    RU: 'id',
  },
  param_nickname: {
    EN: 'Nickname',
    RU: 'Никнейм',
  },
  param_email: {
    EN: 'E-mail',
    RU: 'Эл. почта',
  },
  param_phone: {
    EN: 'Phone',
    RU: 'Телефон',
  },
  param_first_name: {
    EN: 'First name',
    RU: 'Имя',
  },
  param_middle_name: {
    EN: 'Middle name',
    RU: 'Отчество',
  },
  param_last_name: {
    EN: 'Last name',
    RU: 'Фамилия',
  },
  param_gender: {
    EN: 'Gender',
    RU: 'Пол',
  },
  param_date_of_birth: {
    EN: 'Date of birth',
    RU: 'Дата рождения',
  },
  param_country: {
    EN: 'Country',
    RU: 'Страна',
  },
  param_city: {
    EN: 'City',
    RU: 'Город',
  },
  param_company: {
    EN: 'Company',
    RU: 'Компания',
  },
  param_position_held: {
    EN: 'Position held',
    RU: 'Должность',
  },
};

export const T7E_NEVIS_PASSWORD_RESET = {
  title_reset: {
    EN: 'Password reset',
    RU: 'Сброс пароля',
  },
  title_reset_req: {
    EN: 'Password reset request',
    RU: 'Запрос на сброс пароля',
  },
  catcha_title: {
    EN: 'Please confirm that you are a real person:',
    RU: 'Подтвердите, что вы человек:',
  },
  new_password: {
    EN: 'New password',
    RU: 'Новый пароль',
  },
  new_password_repeat: {
    EN: 'Repeat new password',
    RU: 'Повтор нового пароля',
  },
  different_passwords: {
    EN: 'New password and repeat do not match',
    RU: 'Новый пароль и его повтор не совпадают',
  },
  successfully_completed: {
    EN: 'Successfully completed',
    RU: 'Успешно выполнен',
  },
  go_to_login: {
    EN: 'Go to Log-in',
    RU: 'Вход',
  },
  email_associated_with_account: {
    EN: 'E-mail associated with your account',
    RU: 'Эл. почта вашего аккаунта',
  },
  check_email_box: {
    EN: 'Please check your e-mail box',
    RU: 'Пожалуйста проверьте свой почтовый ящик',
  },
};
