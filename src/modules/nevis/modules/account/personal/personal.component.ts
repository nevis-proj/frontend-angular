import {AfterViewInit, Component} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';
import {DwfeCanComponentDeactivate} from '@dwfe/classes/DwfeCanComponentDeactivate';

import {T7E_NEVIS_ACCOUNT_PERSONAL} from '../t7e';
import {NEVIS_COUNTRIES, NEVIS_GENDERS} from '../../../t7e';
import {NevisAccountPersonalBackendEntity} from '../../../classes/NevisAccountPersonalBackendEntity';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-account-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class NevisAccountPersonalComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit, DwfeCanComponentDeactivate {

  subjCancelEdit_Info = new Subject<boolean>();
  subjSaveEdit_Info = new Subject<boolean>();
  subjSaveEdit_Change = new Subject<boolean>();

  cCurrentPassword: AbstractControl;

  cId: AbstractControl;
  cCreatedOn: AbstractControl;

  cNickName: AbstractControl;
  cNewNickName: AbstractControl;
  tNickName: AbstractControl;
  newNickName_changed: boolean;
  nickNameNonPublic_changed: boolean;

  cFirstName: AbstractControl;
  tFirstName: AbstractControl;
  firstName_changed: boolean;
  firstNameNonPublic_changed: boolean;

  cMiddleName: AbstractControl;
  tMiddleName: AbstractControl;
  middleName_changed: boolean;
  middleNameNonPublic_changed: boolean;

  cLastName: AbstractControl;
  tLastName: AbstractControl;
  lastName_changed: boolean;
  lastNameNonPublic_changed: boolean;

  cGender: AbstractControl;
  tGender: AbstractControl;
  gendersList = NEVIS_GENDERS;
  gender_changed: boolean;
  genderNonPublic_changed: boolean;

  cDateOfBirth: AbstractControl;
  tDateOfBirth: AbstractControl;
  dateOfBirth_changed: boolean;
  dateOfBirthNonPublic_changed: boolean;

  cCountry: AbstractControl;
  tCountry: AbstractControl;
  countriesList = NEVIS_COUNTRIES;
  country_changed: boolean;
  countryNonPublic_changed: boolean;

  cCity: AbstractControl;
  tCity: AbstractControl;
  city_changed: boolean;
  cityNonPublic_changed: boolean;

  cCompany: AbstractControl;
  tCompany: AbstractControl;
  company_changed: boolean;
  companyNonPublic_changed: boolean;

  cPositionHeld: AbstractControl;
  tPositionHeld: AbstractControl;
  positionHeld_changed: boolean;
  positionHeldNonPublic_changed: boolean;

  constructor(public nevisService: NevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.cId.setValue(this.nevisService.auth.id);
      this.cId.disable();

      this.cCreatedOn.setValue(DwfeGlobals.formatDate(this.nevisService.auth.createdOn));
      this.cCreatedOn.disable();

      this.nevisService
        .accountPersonal$({
          'initiator': this,
          'responseHandlerFn':
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.nevisService.accountPersonal = data.data;
              } else {
                this.setErrorMessage(data.description);
              }
            }
        })
        .pipe(
          takeUntil(this.latchForUnsubscribe$)
        )
        .subscribe(
          (aPersonal: NevisAccountPersonalBackendEntity) => {
            this.cNickName.setValue(aPersonal.nickName);

            this.cNewNickName.setValue(aPersonal.nickName);
            this.tNickName.setValue(!aPersonal.nickNameNonPublic);

            this.cFirstName.setValue(aPersonal.firstName);
            this.tFirstName.setValue(!aPersonal.firstNameNonPublic);

            this.cMiddleName.setValue(aPersonal.middleName);
            this.tMiddleName.setValue(!aPersonal.middleNameNonPublic);

            this.cLastName.setValue(aPersonal.lastName);
            this.tLastName.setValue(!aPersonal.lastNameNonPublic);

            this.cGender.setValue(aPersonal.gender);
            this.tGender.setValue(!aPersonal.genderNonPublic);

            this.cDateOfBirth.setValue(aPersonal.dateOfBirth);
            this.tDateOfBirth.setValue(!aPersonal.dateOfBirthNonPublic);

            this.cCountry.setValue(aPersonal.country);
            this.tCountry.setValue(!aPersonal.countryNonPublic);

            this.cCity.setValue(aPersonal.city);
            this.tCity.setValue(!aPersonal.cityNonPublic);

            this.cCompany.setValue(aPersonal.company);
            this.tCompany.setValue(!aPersonal.companyNonPublic);

            this.cPositionHeld.setValue(aPersonal.positionHeld);
            this.tPositionHeld.setValue(!aPersonal.positionHeldNonPublic);
          });

      this.cNickName.disable();

      this.resetMessage(this.cCurrentPassword, ['errorMessage', 'successMessage']);

      this.resetMessage(this.tNickName, ['errorMessage', 'successMessage']);
      this.resetMessage(this.cNewNickName, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cFirstName, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tFirstName, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cMiddleName, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tMiddleName, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cLastName, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tLastName, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cGender, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tGender, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cDateOfBirth, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tDateOfBirth, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cCountry, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tCountry, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cCity, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tCity, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cCompany, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tCompany, ['errorMessage', 'successMessage']);

      this.resetMessage(this.cPositionHeld, ['errorMessage', 'successMessage']);
      this.resetMessage(this.tPositionHeld, ['errorMessage', 'successMessage']);

    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  get isInfoChanged(): boolean {
    return this.nickNameNonPublic_changed
      || this.firstName_changed || this.firstNameNonPublic_changed
      || this.middleName_changed || this.middleNameNonPublic_changed
      || this.lastName_changed || this.lastNameNonPublic_changed
      || this.gender_changed || this.genderNonPublic_changed
      || this.dateOfBirth_changed || this.dateOfBirthNonPublic_changed
      || this.country_changed || this.countryNonPublic_changed
      || this.city_changed || this.cityNonPublic_changed
      || this.company_changed || this.companyNonPublic_changed
      || this.positionHeld_changed || this.positionHeldNonPublic_changed;
  }

  performUpdateAccountPersonal() {
    this.activeExchangeUnitNumber = 1;

    this.nevisService
      .updateAccountPersonalExchanger
      .run(this,
        this.getUpdateAccountPersonalRequestBody(),
        (data: DwfeExchangeResult) => {
          if (data.result) {
            this.subjSaveEdit_Info.next(true);
            this.nevisService.accountPersonal = data.data;
            this.successMessage = this.app.t7eMap('personal_success_updated', this.t7e);
          } else {
            this.setErrorMessage(data.description);
          }
        });
  }

  performCancelEdit() {
    this.subjCancelEdit_Info.next(true);
  }

  performChangeNickName() {
    this.activeExchangeUnitNumber = 2;

    this.nevisService
      .nickNameChangeExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          newNickName: this.cNewNickName.value,
          curpass: this.cCurrentPassword.value
        }),
        (data: DwfeExchangeResult) => {
          this.cCurrentPassword.reset();
          if (data.result) {
            this.nevisService.updateAccountPersonal({nickName: this.cNewNickName.value});
            this.subjSaveEdit_Change.next(true);
            this.successMessage = this.app.t7eMap('nick_name_changed', this.t7e);
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  getUpdateAccountPersonalRequestBody(): string {
    const req = {};

    this.addField('nickNameNonPublic', !this.tNickName.value, req);

    this.addField('firstName', this.cFirstName.value, req);
    this.addField('firstNameNonPublic', !this.tFirstName.value, req);

    this.addField('middleName', this.cMiddleName.value, req);
    this.addField('middleNameNonPublic', !this.tMiddleName.value, req);

    this.addField('lastName', this.cLastName.value, req);
    this.addField('lastNameNonPublic', !this.tLastName.value, req);

    this.addField('gender', this.cGender.value, req);
    this.addField('genderNonPublic', !this.tGender.value, req);

    let dateOfBirth = this.cDateOfBirth.value;
    dateOfBirth = dateOfBirth === null ? null : DwfeGlobals.formatDate(dateOfBirth);
    this.addField('dateOfBirth', dateOfBirth, req);
    this.addField('dateOfBirthNonPublic', !this.tDateOfBirth.value, req);

    this.addField('country', this.cCountry.value, req);
    this.addField('countryNonPublic', !this.tCountry.value, req);

    this.addField('city', this.cCity.value, req);
    this.addField('cityNonPublic', !this.tCity.value, req);

    this.addField('company', this.cCompany.value, req);
    this.addField('companyNonPublic', !this.tCompany.value, req);

    this.addField('positionHeld', this.cPositionHeld.value, req);
    this.addField('positionHeldNonPublic', !this.tPositionHeld.value, req);

    return DwfeGlobals.prepareReq(req);
  }

  addField(name, value, req): void {
    if (this[name + '_changed']) {
      req[name] = value;
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.isInfoChanged || this.isLocked) {
      return confirm(this.app.t7eMap('save_changes', this.t7e));
    } else {
      return true;
    }
  }

  get t7e(): any {
    return T7E_NEVIS_ACCOUNT_PERSONAL;
  }
}
