import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeModule} from '@dwfe/dwfe.module';

import {NevisAccountComponent} from './account.component';
import {NevisAccountEmailComponent} from './email/email.component';
import {NevisAccountPasswordNDeleteComponent} from './password-n-delete/password-n-delete.component';
import {NevisAccountPersonalComponent} from './personal/personal.component';
import {NevisAccountPhoneComponent} from './phone/phone.component';

@NgModule({
  imports: [
    CommonModule,

    DwfeModule,
  ],
  declarations: [
    NevisAccountComponent,

    NevisAccountEmailComponent,
    NevisAccountPasswordNDeleteComponent,
    NevisAccountPersonalComponent,
    NevisAccountPhoneComponent,
  ],
  exports: [
    NevisAccountComponent,

    NevisAccountEmailComponent,
    NevisAccountPasswordNDeleteComponent,
    NevisAccountPersonalComponent,
    NevisAccountPhoneComponent,
  ]
})
export class NevisAccountModule {
}
