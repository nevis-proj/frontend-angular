import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';
import {DwfeCanComponentDeactivate} from '@dwfe/classes/DwfeCanComponentDeactivate';

import {T7E_NEVIS_ACCOUNT_EMAIL} from '../t7e';
import {NevisService} from '../../../services/nevis.service';
import {NevisAccountEmailBackendEntity} from '../../../classes/NevisAccountEmailBackendEntity';

@Component({
  selector: 'nevis-account-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class NevisAccountEmailComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit, OnDestroy, DwfeCanComponentDeactivate {

  subjCancelEdit_Info = new Subject<boolean>();
  subjSaveEdit_Info = new Subject<boolean>();
  subjSaveEdit_Change = new Subject<boolean>();

  cEmail: AbstractControl;
  cNewEmail: AbstractControl;
  tEmail: AbstractControl;
  newEmail_changed: boolean;
  emailNonPublic_changed: boolean;

  syncJob: any;

  isSentConfirmReq = false;

  cCurrentPassword: AbstractControl;

  constructor(public nevisService: NevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {

      this.nevisService
        .accountEmail$({
          'initiator': this,
          'responseHandlerFn':
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.nevisService.accountEmail = data.data;
              } else {
                this.cEmail.setValue(null);
                this.tEmail.setValue(false);
                this.cNewEmail.setValue(null);
                this.setErrorMessage(data.description);
              }
            }
        })
        .pipe(
          takeUntil(this.latchForUnsubscribe$)
        )
        .subscribe(
          (aEmail: NevisAccountEmailBackendEntity) => {

            if (aEmail.value !== this.cEmail.value) {
              this.cEmail.setValue(aEmail.value);

              this.cNewEmail.setValue(aEmail.value);
              this.cNewEmail.markAsUntouched();
              this.cNewEmail.markAsPristine();
            }

            if (!aEmail.nonPublic !== this.tEmail.value) {
              this.tEmail.setValue(!aEmail.nonPublic);
            }

            if (aEmail.isSentConfirmReq !== this.isSentConfirmReq) {
              this.isSentConfirmReq = aEmail.isSentConfirmReq;
            }

            if (!this.syncJob && !aEmail.confirmed) {
              this.syncJob = setInterval(() => {
                if (this.nevisService.isEmailConfirmed()) {
                  clearInterval(this.syncJob);
                  this.syncJob = null;
                } else {
                  this.nevisService.syncConfirmedEmailFlagWithStorage();
                }
              }, 2000);
            }
          });

      this.cEmail.disable();

      this.resetMessage(this.cCurrentPassword, ['errorMessage', 'successMessage']);

      this.resetMessage(this.tEmail, ['errorMessage', 'successMessage']);
      this.resetMessage(this.cNewEmail, ['errorMessage', 'successMessage']);

    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.syncJob) {
      clearInterval(this.syncJob);
    }
  }

  get isInfoChanged(): boolean {
    return this.emailNonPublic_changed;
  }

  performCancelEdit() {
    this.subjCancelEdit_Info.next(true);
  }

  performUpdateAccountEmail() {
    this.activeExchangeUnitNumber = 1;

    this.nevisService
      .updateAccountEmailExchanger
      .run(this,
        this.getUpdateAccountEmailRequestBody(),
        (data: DwfeExchangeResult) => {
          if (data.result) {
            this.nevisService.updateAccountEmail({nonPublic: !this.tEmail.value});
            this.subjSaveEdit_Info.next(true);
            this.successMessage = this.app.t7eMap('email_info_success_updated', this.t7e);
          } else {
            this.setErrorMessage(data.description);
          }
        });
  }

  getUpdateAccountEmailRequestBody(): string {
    const req = {};

    if (this.emailNonPublic_changed) {
      req['nonPublic'] = !this.tEmail.value;
    }

    return DwfeGlobals.prepareReq(req);
  }

  performEmailConfirmationReq() {
    this.activeExchangeUnitNumber = 2;

    this.nevisService.refreshAccountEmail({
      'initiator': this,
      'params': {chainExchange: true},
      'responseHandlerFn': (data: DwfeExchangeResult) => {
        if (data.result) {
          this.nevisService.accountEmail = data.data;
          this.nevisService.updateAccountEmail({isSentConfirmReq: true});

          if (data['confirmed']) {
            this.setLocked(false); // because chainExchange
          } else {
            this.nevisService
              .emailConfirmReqExchanger
              .run(this, null,
                (data1: DwfeExchangeResult) => {
                  if (data1.result) {
                    this.errorMessage = '';
                    this.successMessage = this.app.t7eMap('request_sent', this.t7e);
                  } else {
                    this.successMessage = '';
                    this.setErrorMessage(data1.description);
                  }
                  if (!this.isSentConfirmReq) {
                    this.nevisService.updateAccountEmail({isSentConfirmReq: true});
                  }
                }
              );
          }
        } else {
          this.nevisService.updateAccountEmail({isSentConfirmReq: false});
          this.successMessage = '';
          this.setErrorMessage(data.description);
          this.setLocked(false); // because chainExchange
        }
      }
    });
  }

  performEmailChange() {
    this.activeExchangeUnitNumber = 3;
    const newemail = this.cNewEmail.value;

    this.nevisService
      .emailChangeExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          newemail: newemail,
          curpass: this.cCurrentPassword.value
        }),
        (data: DwfeExchangeResult) => {
          this.cCurrentPassword.reset();
          if (data.result) {
            this.nevisService.updateAccountEmail({
              value: newemail,
              confirmed: false,
              isSentConfirmReq: false
            });
            this.nevisService.auth.username = newemail;
            this.subjSaveEdit_Change.next(true);
            this.successMessage = this.app.t7eMap('email_success_changed', this.t7e);
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.isInfoChanged || this.isLocked) {
      return confirm(this.app.t7eMap('save_changes', this.t7e));
    } else {
      return true;
    }
  }

  get t7e(): any {
    return T7E_NEVIS_ACCOUNT_EMAIL;
  }
}
