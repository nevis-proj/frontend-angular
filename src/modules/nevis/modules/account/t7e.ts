export const T7E_NEVIS_ACCOUNT = {
  tab_personal: {
    EN: 'Personal',
    RU: 'Личные',
  },
  tab_email: {
    EN: 'E-mail',
    RU: 'Эл. почта',
  },
  tab_phone: {
    EN: 'Phone',
    RU: 'Телефон',
  },
  tab_password_n_delete: {
    EN: 'Password & Delete',
    RU: 'Пароль & Удаление',
  },
};

export const T7E_NEVIS_ACCOUNT_PHONE = {
  title_info: {
    EN: 'Phone information',
    RU: 'Информация о телефоне',
  },
  title_change: {
    EN: 'Change phone',
    RU: 'Сменить телефон',
  },
  hint_true: {
    EN: 'Pubic',
    RU: 'Публично',
  },
  hint_false: {
    EN: 'Hidden',
    RU: 'Скрыто',
  },
  phone: {
    EN: 'Phone',
    RU: 'Телефон',
  },
  new_phone: {
    EN: 'New phone',
    RU: 'Новый телефон',
  },
  current_password: {
    EN: 'Your current password',
    RU: 'Ваш текущий пароль',
  },
  info_success_updated: {
    EN: 'Phone info has been successfully updated',
    RU: 'Информация о телефоне успешно обновлена',
  },
  phone_success_changed: {
    EN: 'Phone has been successfully changed',
    RU: 'Телефон успешно изменен',
  },
  save_changes: {
    EN: 'Your changes have not been saved. Do you want to leave the page?',
    RU: 'Ваши изменения не сохранены. Хотите покинуть страницу?',
  },
};

export const T7E_NEVIS_ACCOUNT_EMAIL = {
  title_info: {
    EN: 'E-mail information',
    RU: 'Информация о эл. почте',
  },
  title_confirm: {
    EN: 'E-mail confirm',
    RU: 'Подтверждение эл. почты',
  },
  title_change: {
    EN: 'Change e-mail',
    RU: 'Сменить эл. почту',
  },
  hint_true: {
    EN: 'Pubic',
    RU: 'Публично',
  },
  hint_false: {
    EN: 'Hidden',
    RU: 'Скрыто',
  },
  email: {
    EN: 'E-mail',
    RU: 'Эл. почта',
  },
  check_email_box: {
    EN: 'Please check your e-mail box',
    RU: 'Пожалуйста проверьте свой почтовый ящик',
  },
  i_want_to_retry_req: {
    EN: 'I want to retry the request',
    RU: 'Повторить запрос',
  },
  run_confirm: {
    EN: 'Run confirmation',
    RU: 'Выполнить подтверждение',
  },
  email_confirmed: {
    EN: 'Your e-mail is confirmed',
    RU: 'Ваша эл. почта подтверждена',
  },
  new_email: {
    EN: 'New e-mail',
    RU: 'Новая эл. почта',
  },
  current_password: {
    EN: 'Your current password',
    RU: 'Ваш текущий пароль',
  },
  email_info_success_updated: {
    EN: 'E-mail info has been successfully updated',
    RU: 'Информация об эл. почте была успешно обновлена',
  },
  request_sent: {
    EN: 'The request has been sent',
    RU: 'Запрос отправлен',
  },
  email_success_changed: {
    EN: 'E-mail has been successfully changed',
    RU: 'Эл. почта была успешно изменена',
  },
  save_changes: {
    EN: 'Your changes have not been saved. Do you want to leave the page?',
    RU: 'Ваши изменения не сохранены. Хотите покинуть страницу?',
  },
};

export const T7E_NEVIS_ACCOUNT_PASSWORD_N_DELETE = {
  different_passwords: {
    EN: 'New password and repeat do not match',
    RU: 'Новый пароль и его повтор не совпадают',
  },
  success_changed: {
    EN: 'Your password has been successfully changed',
    RU: 'Ваш пароль был успешно изменен',
  },
  title_change_password: {
    EN: 'Change password',
    RU: 'Сменить пароль',
  },
  new_password: {
    EN: 'New password',
    RU: 'Новый пароль',
  },
  new_password_repeat: {
    EN: 'Repeat new password',
    RU: 'Повтор нового пароля',
  },
  current_password: {
    EN: 'Your current password',
    RU: 'Ваш текущий пароль',
  },
  delete_account: {
    EN: 'Delete account',
    RU: 'Удаление аккаунта',
  },
  delete_forever: {
    EN: 'Delete my account forever',
    RU: 'Удалить мой аккаунт навсегда',
  },
};

export const T7E_NEVIS_ACCOUNT_PERSONAL = {
  title_info: {
    EN: 'Personal information',
    RU: 'Личная информация',
  },
  title_change: {
    EN: 'Change nickname',
    RU: 'Сменить никнейм',
  },
  hint_true: {
    EN: 'Pubic',
    RU: 'Публично',
  },
  hint_false: {
    EN: 'Hidden',
    RU: 'Скрыто',
  },
  nick_name: {
    EN: 'Nickname',
    RU: 'Никнейм',
  },
  first_name: {
    EN: 'First name',
    RU: 'Имя',
  },
  middle_name: {
    EN: 'Middle name',
    RU: 'Отчество',
  },
  last_name: {
    EN: 'Last name',
    RU: 'Фамилия',
  },
  gender: {
    EN: 'Gender',
    RU: 'Пол',
  },
  date_of_birth: {
    EN: 'Date of birth',
    RU: 'Дата рождения',
  },
  country: {
    EN: 'Country',
    RU: 'Страна',
  },
  city: {
    EN: 'City',
    RU: 'Город',
  },
  company: {
    EN: 'Company',
    RU: 'Компания',
  },
  position_held: {
    EN: 'Position held',
    RU: 'Должность',
  },
  new_nickname: {
    EN: 'New nickname',
    RU: 'Новый никнейм',
  },
  current_password: {
    EN: 'Your current password',
    RU: 'Ваш текущий пароль',
  },
  other: {
    EN: 'Other',
    RU: 'Другое',
  },
  created_on: {
    EN: 'Account created on',
    RU: 'Аккаунт создан',
  },
  public_page: {
    EN: 'Your account public page',
    RU: 'Публичная страница вашего аккаунта',
  },
  personal_success_updated: {
    EN: 'Personal info has been successfully updated',
    RU: 'Личная информация успешно обновлена',
  },
  nick_name_changed: {
    EN: 'Nickname has been successfully changed',
    RU: 'Никнейм успешно изменен',
  },
  save_changes: {
    EN: 'Your changes have not been saved. Do you want to leave the page?',
    RU: 'Ваши изменения не сохранены. Хотите покинуть страницу?',
  },
};
