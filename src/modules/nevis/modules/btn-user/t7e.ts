export const T7E_NEVIS_BTN_USER_LOGGED_IN = {
  logged_in_as: {
    EN: 'logged in as',
    RU: 'вошли как',
  },
  account: {
    EN: 'Account',
    RU: 'Аккаунт',
  },
  log_out: {
    EN: 'Log out',
    RU: 'Выход',
  },
};

export const T7E_NEVIS_BTN_USER_NOT_AUTHENTICATED = {
  log_in: {
    EN: 'Log in',
    RU: 'Вход',
  }
};
