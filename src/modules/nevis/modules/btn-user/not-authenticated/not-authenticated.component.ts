import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {T7E_NEVIS_BTN_USER_NOT_AUTHENTICATED} from '../t7e';

@Component({
  selector: 'nevis-btn-user-not-authenticated',
  templateUrl: './not-authenticated.component.html',
})
export class NevisBtnUserNotAuthenticatedComponent {

  constructor(public router: Router,
              public app: AppService) {
  }

  goToLogin(): void {
    setTimeout(() => {
      this.router.navigateByUrl('/auth/login');
    }, 70);
  }

  get t7e(): any {
    return T7E_NEVIS_BTN_USER_NOT_AUTHENTICATED;
  }
}
