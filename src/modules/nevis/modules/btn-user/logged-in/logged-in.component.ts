import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {animate, style, transition, trigger} from '@angular/animations';

import {AppService} from '@app/services/app.service';

import {T7E_NEVIS_BTN_USER_LOGGED_IN} from '../t7e';
import {NevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-btn-user-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.scss'],
  animations: [
    trigger('animatedBlock', [  // https://angular.io/guide/animations#example-entering-and-leaving
      transition(':enter', [    // https://angular.io/api/animations/transition#using-enter-and-leave
        style({opacity: 0}),
        animate('120ms ease-in', style({opacity: 1}))
      ]),
      transition(':leave', [
        animate('120ms ease-out', style({opacity: 0}))
        // animate('150ms ease-out', style({opacity: 0, transform: 'translateX(100%)'}))
      ]),
    ])
  ],
})
export class NevisBtnUserLoggedInComponent {

  isMenuOpen = false;

  constructor(public nevisService: NevisService,
              public router: Router,
              public app: AppService) {
  }

  myAccount(): void {
    this.router.navigateByUrl('/cp');
    this.isMenuOpen = false;
  }

  get t7e(): any {
    return T7E_NEVIS_BTN_USER_LOGGED_IN;
  }
}
